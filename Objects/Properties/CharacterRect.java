package com.moosader.yob.Objects.Properties;

import android.graphics.Rect;

public class CharacterRect {
    private int x;
    private int y;
    private int width;
    private int height;
    private Rect rect = new Rect();

    public CharacterRect( int x, int y, int width, int height ) {
        setRect( x, y, width, height );
    }

    public void setRect( int x, int y, int width, int height ) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void setPosition( int x, int y ) {
        this.x = x;
        this.y = y;
    }

    public void setDimensions( int width, int height ) {
        this.width = width;
        this.height = height;
    }

    public void setX( int x ) {
        this.x = x;
    }

    public void setY( int y ) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Rect getRect() {
        rect.left = x;
        rect.top = y;
        rect.right = x + width;
        rect.bottom = y + height;

        return rect;
    }
}
