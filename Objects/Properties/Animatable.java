package com.moosader.yob.Objects.Properties;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Animatable {
    private Drawable drawable = new Drawable();

    private int maxFrames;  // columns
    private int maxActions; // rows
    private double frame;   // columns
    private double action;  // rows
    private double animateSpeed;

    private int frameWidth;
    private int frameHeight;

    public Animatable() {

    }

    public void setup( Bitmap image, int rows, int columns, double animateSpeed ) {
        this.drawable.setImage(image);
        this.maxActions = rows;
        this.maxFrames = columns;
        this.frame = 0.0;
        this.action = 0.0;
        this.animateSpeed = animateSpeed;

        this.frameWidth = this.drawable.getWidth() / maxFrames;
        this.frameHeight = this.drawable.getHeight() / maxActions;

        drawable.setFrame( 0, 0, frameWidth, frameHeight );
        drawable.setPosition( 0, 0, frameWidth, frameHeight );
    }

    public void setAction( int action ) {
        this.action = action;
    }

    public void animate() {
        frame += animateSpeed;
        if ( frame >= maxFrames ) {
            frame = 0;
        }
        drawable.setFramePosition( (int)frame * frameWidth, (int)action * frameHeight );
    }

    public void setPosition( int x, int y ) {
        drawable.setPosition( x, y );
    }

    public int getX() {
        return drawable.getX();
    }

    public int getY() {
        return drawable.getY();
    }

    public void setX( int x ) {
        drawable.setX( x );
    }

    public void setY( int y ) {
        drawable.setY( y );
    }

    public void draw( Canvas canvas ) {
        //canvas.drawBitmap( sprites[(int)frame][(int)action], drawable.getX(), drawable.getY(), null );
        drawable.draw( canvas );
    }
}
