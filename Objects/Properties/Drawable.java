package com.moosader.yob.Objects.Properties;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

public class Drawable {
    private Bitmap image;
    private CharacterRect position;
    private CharacterRect frame;


    public Drawable() {
        position = new CharacterRect( 0, 0, 0, 0 );
        frame = new CharacterRect( 0, 0, 40, 40 );
        image = null;
    }

    public void setImage(Bitmap image){
        this.image = image;
    }

    public void setImage( Bitmap bitmap, int subX, int subY, int subWidth, int subHeight )
    {
        frame.setRect( subX, subY, subWidth, subHeight );
        setImage( image );
    }

    public void setPosition( int x, int y, int width, int height ) {
        position.setRect( x, y, width, height );
    }

    public void setPosition( int x, int y ) {
        position.setPosition( x, y );
    }

    public void setDimensions( int width, int height ) {
        position.setDimensions( width, height );
    }

    public void setFrame( int x, int y, int width, int height ) {
        frame.setRect( x, y, width, height );
    }

    public void setFramePosition( int x, int y ) {
        frame.setPosition( x, y );
    }

    public void setFrameDimensions( int width, int height ) {
        frame.setDimensions( width, height );
    }

    public void setX( int x ) {
        position.setX( x );
    }

    public void setY( int y ) {
        position.setY( y );
    }

    public int getX() {
        return position.getX();
    }

    public int getY() {
        return position.getY();
    }

    public int getWidth() {
        return position.getWidth();
    }

    public int getHeight() {
        return position.getHeight();
    }

    public Bitmap getImage() {
        return this.image;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(this.image, frame.getRect(), position.getRect(), null);
    }

    public void debug() {
        Rect posRect = position.getRect();
        Rect frameRect = frame.getRect();

        Log.i( "Position X", Integer.toString( position.getX() ) );
        Log.i( "Position Y", Integer.toString( position.getY() ) );
        Log.i( "Position WIDTH", Integer.toString( position.getWidth() ) );
        Log.i( "Position HEIGHT", Integer.toString( position.getHeight() ) );
        Log.i( "Position LEFT", Integer.toString( posRect.left ) );
        Log.i( "Position RIGHT", Integer.toString( posRect.right ) );
        Log.i( "Position TOP", Integer.toString( posRect.top ) );
        Log.i( "Position BOTTOM", Integer.toString( posRect.bottom ) );

        Log.i( "Frame X", Integer.toString( frame.getX() ) );
        Log.i( "Frame Y", Integer.toString( frame.getY() ) );
        Log.i( "Frame WIDTH", Integer.toString( frame.getWidth() ) );
        Log.i( "Frame HEIGHT", Integer.toString( frame.getHeight() ) );
        Log.i( "Frame LEFT", Integer.toString( frameRect.left ) );
        Log.i( "Frame RIGHT", Integer.toString( frameRect.right ) );
        Log.i( "Frame TOP", Integer.toString( frameRect.top ) );
        Log.i( "Frame BOTTOM", Integer.toString( frameRect.bottom ) );
    }
}
