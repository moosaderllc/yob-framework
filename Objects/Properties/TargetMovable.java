package com.moosader.yob.Objects.Properties;

public class TargetMovable {
    private int x;
    private int y;
    private int targetX;
    private int targetY;
    private int targetThreshold;
    private int speed;
    private boolean isMoving;

    public TargetMovable() {
        x = 0;
        y = 0;
        targetX = 0;
        targetY = 0;
        targetThreshold = 10;
        isMoving = false;
        speed = 1;
    }

    public void setup( int x, int y, int speed ) {
        setPosition( x, y );
        setTarget( x, y );
        setSpeed( speed );
    }

    public int getXDirection()
    {
        if      ( targetX + targetThreshold < x ) { return -1; }
        else if ( targetX - targetThreshold > x ) { return 1; }
        return 0;
    }

    public int getYDirection()
    {
        if      ( targetY + targetThreshold < y ) { return -1; }
        else if ( targetY - targetThreshold > y ) { return 1; }
        return 0;
    }

    public void setX( int x ) {
        this.x = x;
    }

    public void setY( int y ) {
        this.y = y;
    }

    public void setPosition( int x, int y ) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Set the target coordinates for where the player wants to move
     * @param x coordinate
     * @param y coordinate
     */
    public void setTarget( int x, int y ) {
        targetX = x;
        targetY = y;
    }

    public void setSpeed( int speed ) {
        this.speed = speed;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void update() {
        move( getXDirection(), getYDirection() );
    }

    public void keepInRegion( int minX, int minY, int maxX, int maxY ) {
        if      ( x < minX ) { x = minX; }
        else if ( x > maxX ) { x = maxX; }
        if      ( y < minY ) { y = minY; }
        else if ( y > maxY ) { y = maxY; }
    }

    /**
     * Handles player movement
     * @param horiz -1 for left, 0 for no horizontal movement, 1 for right
     * @param vert -1 for up, 0 for no vertical movement, 1 for down
     */
    public void move( int horiz, int vert ) {
        x += speed * horiz;
        y += speed * vert;
    }
}
