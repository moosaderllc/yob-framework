package com.moosader.yob.Objects;

import android.graphics.Canvas;

import com.moosader.yob.Objects.Properties.Animatable;
import com.moosader.yob.Objects.Properties.TargetMovable;

public class TapCharacter {
    private Animatable animatable = new Animatable();
    private TargetMovable movable = new TargetMovable();
    private String name = new String();

    public void setup( String name ) {
        this.name = name;
    }

    public Animatable getAnimatable() {
        return animatable;
    }

    public TargetMovable getMovable() {
        return movable;
    }

    public String getName() {
        return name;
    }

    public void update() {
        movable.update();
        animatable.animate();
    }

    public void draw( Canvas canvas ) {
        //this.animatable.setPosition( movable.getX(), movable.getY() );
        this.animatable.draw(canvas);
    }
}
