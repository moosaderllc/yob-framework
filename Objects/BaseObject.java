package com.moosader.yob.Objects;
import android.graphics.Canvas;

import com.moosader.yob.Objects.Properties.Drawable;

public class BaseObject {
    private Drawable drawable = new Drawable();

    public BaseObject() {
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void draw(Canvas canvas) {
        drawable.draw(canvas);
    }
}