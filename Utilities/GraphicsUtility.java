package com.moosader.yob.Utilities;

import android.content.Context;
import android.util.Log;

public class GraphicsUtility {
    private static float dpi;
    private static int screenWidth;
    private static int screenHeight;
    private static int gameWidth;
    private static int gameHeight;

    public static void setup( Context context, int screenWidth, int screenHeight, int gameWidth, int gameHeight ) {
        dpi = context.getResources().getDisplayMetrics().density;
        Log.i( "Device dpi", Float.toString( dpi ) );

        GraphicsUtility.screenWidth = screenWidth;
        GraphicsUtility.screenHeight = screenHeight;

        Log.i( "Screen Width", Float.toString( GraphicsUtility.screenWidth ) );
        Log.i( "Screen Height", Float.toString( GraphicsUtility.screenHeight ) );

        GraphicsUtility.gameWidth = gameWidth;
        GraphicsUtility.gameHeight = gameHeight;

        Log.i( "Game Width", Float.toString( GraphicsUtility.gameWidth ) );
        Log.i( "Game Height", Float.toString( GraphicsUtility.gameHeight ) );

        Log.i( "Resize Ratio", Float.toString( GraphicsUtility.getRatio() ) );
    }

    public static int getScreenWidth() {
        return GraphicsUtility.screenWidth;
    }

    public static int getScreenHeight() {
        return GraphicsUtility.screenHeight;
    }

    public static int getGameWidth() {
        return GraphicsUtility.gameWidth;
    }

    public static int getGameHeight() {
        return GraphicsUtility.gameHeight;
    }

    /* Conversions */

    public static int dpToPx( float dp ) {
        return (int)(dp / dpi);
    }

    public static float pxToDp( int px ) {
        return px * dpi;
    }

    public static float getRatio() {
        return (float)GraphicsUtility.screenWidth / (float)GraphicsUtility.gameWidth;
    }
}
